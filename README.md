[![Build Status](https://travis-ci.org/brailleapps/braille-utils.basic-cli.svg?branch=master)](https://travis-ci.org/brailleapps/braille-utils.basic-cli)
[![Type](https://img.shields.io/badge/type-library_bundle-blue.svg)](https://github.com/brailleapps/wiki/wiki/Types)

# Basic CLI #
Provides a module for building a basic cli. This module is not a command line interface in it self.

## Using ##
Download the [latest release](http://search.maven.org/#search%7Cga%7C1%7Ca%3A%22braille-utils.basic-cli%22) from maven central.

Extend the `AbstractUI` class to create a cli.

## Building ##
Build with `gradlew build` (Windows) or `./gradlew build` (Mac/Linux)

## Testing ##
Tests are run with `gradlew test` (Windows) or `./gradlew test` (Mac/Linux)

## Requirements & Compatibility ##
- Requires Java 8
- Not compatible with OSGi

## Javadoc ##
Javadoc for the latest Basic CLI is available [here](http://brailleapps.github.io/braille-utils.basic-cli/latest/javadoc/).

## More information ##
See the [common wiki](https://github.com/brailleapps/wiki/wiki) for more information.
